/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.cafeconpalito.Main;

/**
 *
 * @author Albano Díez de Paulino
 */
public class Tarea6 {

    public static void main(String[] args) {
        /*Realiza un proyecto en Netbeans que lea los parámetros o propiedades de conexión de 
un fichero. 
El fichero configuración.props tiene almacenadas las siguientes propiedades:
         */
        
        //Ejercicio1.metodoLeer();
        
        /*Realiza un proyecto en Netbeans que permita a los usuarios logearse en MySQL a 
través de un formulario. 
Cuando un usuario se loguea, comprueba si el nombre existe en la tabla usuarios y si la 
contraseña es correcta. Si la contraseña no es correcta, avisa del mensaje. Si el usuario 
no existe, avisa del mensaje.
El formulario tiene dos botones, uno para el LOGIN y otro para CANCELAR que limpia
los JText*/
        
        Ejercicio2 ej = new Ejercicio2();
        ej.setLocationRelativeTo(null);
        ej.setSize(530, 500);
        ej.setVisible(true);
    }
}
