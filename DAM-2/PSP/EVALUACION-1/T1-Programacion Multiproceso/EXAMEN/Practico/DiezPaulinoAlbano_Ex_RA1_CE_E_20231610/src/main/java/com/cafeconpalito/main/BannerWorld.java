package com.cafeconpalito.main;

/**
 *
 * @author Albano Díez de Paulino
 */
public class BannerWorld {
    
    private static String banner1="Albano Díez  Examen Practico RA1 16/10/2023";
    private static String banner2="Generando fichero con Generator…";
    private static String banner3="Ejecutando la copia con el comando copy";
    private static String banner4="Copia realizada con éxito";
    private static String banner5="Procediendo a abrir el fichero copiado";
    private static String banner6="Error en la copia";

    public static String getBanner1() {
        return banner1;
    }

    public static String getBanner2() {
        return banner2;
    }

    public static String getBanner3() {
        return banner3;
    }

    public static String getBanner4() {
        return banner4;
    }

    public static String getBanner5() {
        return banner5;
    }

    public static String getBanner6() {
        return banner6;
    }
    
    

}
