POKEDEX REALIZADA POR ALBANO DÍEZ DE PAULINO
CAFE CON PALITO®

Para ejecutar la aplicación es necesario tener instalado la maquina virtual de java 8
y tener conexion a internet

https://www.java.com/es/download/ie_manual.jsp

Se debe ejecutar el fichero pokedex-1.0-jar-with-dependencies.jar dando doble click izquierdo o
ejecutando el siguiente comando:
java -jar pokedex-1.0-jar-with-dependencies.jar [id_pokemon:int]{1,1025}