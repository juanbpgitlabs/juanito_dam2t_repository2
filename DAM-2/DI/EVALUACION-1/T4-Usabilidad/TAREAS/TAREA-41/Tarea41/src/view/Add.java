/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package view;

import javax.swing.JPanel;

/**
 *
 * @author carra
 */
public class Add extends javax.swing.JDialog {

    /**
     * Creates new form Add
     */
    public Add(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
       
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fondo = new javax.swing.JPanel();
        copyright = new javax.swing.JLabel();
        formulario = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Module Add Game");

        fondo.setBackground(new java.awt.Color(42, 71, 94));
        fondo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        copyright.setFont(new java.awt.Font("Roboto Medium", 0, 14)); // NOI18N
        copyright.setForeground(new java.awt.Color(255, 255, 255));
        copyright.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        copyright.setText("<html>\n<body>\n© 2023 Valve Corporation. Todos los derechos reservados. <br>\nTodas las marcas registradas pertenecen a sus respectivos dueños en EE. UU. y otros países.<br>\n Todos los precios incluyen IVA (donde sea aplicable). \n</body>\n</html"); // NOI18N
        copyright.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        copyright.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                copyrightMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                copyrightMouseExited(evt);
            }
        });
        fondo.add(copyright, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 660, 700, 70));

        formulario.setBackground(new java.awt.Color(27, 40, 56));

        javax.swing.GroupLayout formularioLayout = new javax.swing.GroupLayout(formulario);
        formulario.setLayout(formularioLayout);
        formularioLayout.setHorizontalGroup(
            formularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 700, Short.MAX_VALUE)
        );
        formularioLayout.setVerticalGroup(
            formularioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 510, Short.MAX_VALUE)
        );

        fondo.add(formulario, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 140, 700, 510));

        jPanel1.setBackground(new java.awt.Color(23, 26, 33));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Roboto Medium", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Add Game to Catalog");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(42, 36, -1, 60));

        fondo.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 700, 140));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(fondo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(fondo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void copyrightMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_copyrightMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_copyrightMouseEntered

    private void copyrightMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_copyrightMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_copyrightMouseExited

    public JPanel getFormulario() {
        return formulario;
    }

    public void setFormulario(JPanel formulario) {
        this.formulario = formulario;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel copyright;
    private javax.swing.JPanel fondo;
    private javax.swing.JPanel formulario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
