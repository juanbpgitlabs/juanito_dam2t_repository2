/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package componentesemaforo;

/**
 *
 * @author carra
 */
public class Main extends javax.swing.JFrame {

    /**
     * Creates new form Main
     */
    public Main() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        miBotonColores1 = new componentesemaforo.MiBotonColores();
        jLabel1 = new javax.swing.JLabel();
        miBotonColores2 = new componentesemaforo.MiBotonColores();
        jLabel2 = new javax.swing.JLabel();
        miBotonColores3 = new componentesemaforo.MiBotonColores();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("DI_RA3_Diez_Paulino_Albano.zip");
        setResizable(false);

        miBotonColores1.setText("ALTO");
        miBotonColores1.setColorTexto(new java.awt.Color(255, 0, 0));
        miBotonColores1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        miBotonColores1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miBotonColores1ActionPerformed(evt);
            }
        });

        jLabel1.setText("*.*.*.*.*.*.*.*.*.*.*.*");

        miBotonColores2.setBackground(new java.awt.Color(255, 255, 51));
        miBotonColores2.setText("PRECAUCION");
        miBotonColores2.setColorTexto(new java.awt.Color(255, 255, 0));
        miBotonColores2.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        miBotonColores2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miBotonColores2ActionPerformed(evt);
            }
        });

        jLabel2.setText("*.*.*.*.*.*.*.*.*.*.*.*");

        miBotonColores3.setBackground(new java.awt.Color(51, 255, 51));
        miBotonColores3.setText("PUEDE PASAR");
        miBotonColores3.setColorTexto(new java.awt.Color(51, 255, 51));
        miBotonColores3.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        miBotonColores3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miBotonColores3ActionPerformed(evt);
            }
        });

        jLabel3.setText("*.*.*.*.*.*.*.*.*.*.*.*");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(miBotonColores3, javax.swing.GroupLayout.PREFERRED_SIZE, 433, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(miBotonColores2, javax.swing.GroupLayout.PREFERRED_SIZE, 433, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
                        .addComponent(miBotonColores1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(63, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(miBotonColores1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(miBotonColores2, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(miBotonColores3, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(66, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void miBotonColores1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miBotonColores1ActionPerformed
        jLabel1.setText("Los vehículos que circulan por el carril deben de detenerse");
        jLabel1.setForeground(miBotonColores1.getColorTexto());
    }//GEN-LAST:event_miBotonColores1ActionPerformed

    private void miBotonColores2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miBotonColores2ActionPerformed
        jLabel2.setText("Si es intermitente circule con precaución,si es fija pare");
        jLabel2.setForeground(miBotonColores2.getColorTexto());
    }//GEN-LAST:event_miBotonColores2ActionPerformed

    private void miBotonColores3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miBotonColores3ActionPerformed
        jLabel3.setText("Los vehiculos que circulan por el carril sobre el cual está situado han de parar");
        jLabel3.setForeground(miBotonColores3.getColorTexto());
    }//GEN-LAST:event_miBotonColores3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private componentesemaforo.MiBotonColores miBotonColores1;
    private componentesemaforo.MiBotonColores miBotonColores2;
    private componentesemaforo.MiBotonColores miBotonColores3;
    // End of variables declaration//GEN-END:variables
}
